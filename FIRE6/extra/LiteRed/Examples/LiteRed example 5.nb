(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 8.0' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       157,          7]
NotebookDataLength[     27307,        702]
NotebookOptionsPosition[     25926,        651]
NotebookOutlinePosition[     26271,        666]
CellTagsIndexPosition[     26228,        663]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{

Cell[CellGroupData[{
Cell["Three-loop on-shell vertex", "Title",
 CellChangeTimes->{{3.554301465671934*^9, 3.5543014667206793`*^9}, {
   3.5563005990081844`*^9, 3.5563006152061844`*^9}, {3.558332003501992*^9, 
   3.558332004266392*^9}, 3.558332265909592*^9}],

Cell[BoxData[
 GraphicsBox[{
   {Dashing[{Medium, Medium}], 
    LineBox[{{0.2944444444444445, 0.8638888888888889}, {0.9111111111111113, 
     0.5}}]}, 
   {Dashing[{Medium, Medium}], 
    LineBox[{{0.9111111111111112, 0.5}, {0.2944444444444444, 
     0.12222222222222201`}}]}, 
   {Dashing[{Medium, Medium}], 
    LineBox[{{0.2944444444444445, 0.8638888888888889}, {0.588888888888889, 
     0.2999999999999998}}]}, 
   {Dashing[{Medium, Medium}], 
    LineBox[{{0.588888888888889, 0.688888888888889}, {0.2944444444444445, 
     0.1222222222222219}}]}, 
   {AbsolutePointSize[6], PointBox[{0.588888888888889, 0.688888888888889}]}, 
   {AbsolutePointSize[6], PointBox[{0.588888888888889, 0.2999999999999997}]}, 
   {AbsolutePointSize[6], 
    PointBox[{0.2944444444444445, 0.12222222222222168`}]}, 
   {AbsolutePointSize[6], PointBox[{0.9111111111111113, 0.5000000000000001}]}, 
   {AbsolutePointSize[6], PointBox[{0.2944444444444445, 0.8638888888888888}]}, 
   {Dashing[{Medium, Medium}], 
    LineBox[{{0.17777777777777778`, 0.9444444444444446}, {
     0.29444444444444445`, 0.8638888888888889}}]}, 
   {Dashing[{Medium, Medium}], 
    LineBox[{{0.2944444444444445, 0.12222222222222179`}, {0.1777777777777778, 
     0.058333333333333126`}}]}, 
   {AbsoluteThickness[3], 
    LineBox[{{0.9111111111111112, 0.5}, {1.0916666666666668`, 0.5}}]}, 
   InsetBox[
    StyleBox[
     TagBox["p",
      HoldForm],
     TextAlignment->Center,
     FontSize->12,
     FontSlant->"Italic",
     Background->GrayLevel[1.]], {0.17643229166666674`, 0.8638888888888889}, {
    Left, Baseline},
    Alignment->{Left, Top}], InsetBox[
    StyleBox[
     TagBox["q",
      HoldForm],
     TextAlignment->Center,
     FontSize->12,
     FontSlant->"Italic",
     Background->GrayLevel[1.]], {0.18815104166666674`, 0.125}, {
    Left, Baseline},
    Alignment->{Left, Top}], InsetBox[
    StyleBox[
     TagBox["",
      HoldForm],
     TextAlignment->Center,
     FontSize->12,
     FontSlant->"Italic",
     Background->GrayLevel[1.]], {0.39518229166666674`, 0.65234375}, {
    Left, Baseline},
    Alignment->{Left, Top}], InsetBox[
    StyleBox[
     TagBox["4",
      HoldForm],
     TextAlignment->Center,
     FontSize->12,
     FontSlant->"Italic",
     Background->GrayLevel[1.]], {0.7233072916666667, 0.59375}, {
    Left, Baseline},
    Alignment->{Left, Top}], InsetBox[
    StyleBox[
     TagBox["5",
      HoldForm],
     TextAlignment->Center,
     FontSize->12,
     FontSlant->"Italic",
     Background->GrayLevel[1.]], {0.7233072916666667, 0.373046875}, {
    Left, Baseline},
    Alignment->{Left, Top}], InsetBox[
    StyleBox[
     TagBox["6",
      HoldForm],
     TextAlignment->Center,
     FontSize->12,
     FontSlant->"Italic",
     Background->GrayLevel[1.]], {0.33268229166666674`, 0.75}, {
    Left, Baseline},
    Alignment->{Left, Top}], InsetBox[
    StyleBox[
     TagBox["7",
      HoldForm],
     TextAlignment->Center,
     FontSize->12,
     FontSlant->"Italic",
     Background->GrayLevel[1.]], {0.5240885416666667, 0.564453125}, {
    Left, Baseline},
    Alignment->{Left, Top}], InsetBox[
    StyleBox[
     TagBox["8",
      HoldForm],
     TextAlignment->Center,
     FontSize->12,
     FontSlant->"Italic",
     Background->GrayLevel[1.]], {0.33268229166666674`, 0.20703125}, {
    Left, Baseline}, {0.0234375, 0.05859374999999999}, {{1., 0.}, {0., 1.}},
    Alignment->{Left, Top}], InsetBox[
    StyleBox[
     TagBox["9",
      HoldForm],
     TextAlignment->Center,
     FontSize->12,
     FontSlant->"Italic",
     Background->GrayLevel[1.]], {0.5221354166666667, 0.373046875}, {
    Left, Baseline},
    Alignment->{Left, Top}], 
   {Dashing[{Medium, Medium}], 
    LineBox[{{0.38736979166666674`, 0.69921875}, {0.38736979166666674`, 
     0.2999999999999998}}]}, 
   {AbsolutePointSize[6], 
    PointBox[{0.38736979166666674`, 0.688888888888889}]}, 
   {AbsolutePointSize[6], 
    PointBox[{0.38736979166666674`, 0.2999999999999997}]}, InsetBox[
    StyleBox[
     RowBox[{
      TagBox["1",
       HoldForm], "1"}],
     TextAlignment->Center,
     FontSize->12,
     FontSlant->"Italic",
     Background->GrayLevel[1.]], {0.36002604166666596`, 0.482421875}, {
    Left, Baseline}, {0.08593749999999982, 0.05859374999999999}, {{1., 0.}, {
    0., 1.}},
    Alignment->{Left, Top}], InsetBox[
    StyleBox[
     TagBox["10",
      HoldForm],
     TextAlignment->Center,
     FontSize->12,
     FontSlant->"Italic",
     Background->GrayLevel[1.]], {0.44986979166666674`, 0.75}, {
    Left, Baseline},
    Alignment->{Left, Top}], InsetBox[
    StyleBox[
     TagBox["12",
      HoldForm],
     TextAlignment->Center,
     FontSize->12,
     FontSlant->"Italic",
     Background->GrayLevel[1.]], {0.44986979166666674`, 0.20703125}, {
    Left, Baseline},
    Alignment->{Left, Top}]},
  ContentSelectable->True,
  ImagePadding->{{0., 0.}, {0., 0.}},
  ImageSize->{349., 256.},
  PlotRange->{{0., 1.3333333333333335`}, {0., 1.}},
  PlotRangePadding->Automatic]], "Input",
 CellChangeTimes->{{3.5590400126572*^9, 3.5590400685987997`*^9}, {
  3.5590401068344*^9, 3.5590401413728*^9}, {3.5590401814648*^9, 
  3.5590402748308*^9}, {3.5625490297800927`*^9, 3.5625491168040705`*^9}, {
  3.564110238727485*^9, 3.5641102690539384`*^9}}],

Cell["First three indices correspond to numerators.", "Text",
 CellChangeTimes->{{3.564110271128742*^9, 3.5641102738743467`*^9}}],

Cell[CellGroupData[{

Cell["Finding reduction rules", "Section",
 CellChangeTimes->{{3.5563006288211846`*^9, 3.5563006412801847`*^9}, 
   3.5563047894731846`*^9, {3.5563874491311193`*^9, 3.5563874694263287`*^9}, {
   3.556387654361615*^9, 3.556387654580011*^9}, {3.5581615194226*^9, 
   3.5581615214086*^9}, {3.5581950641949778`*^9, 3.5581950794205775`*^9}, {
   3.5583319931747923`*^9, 3.558331997059192*^9}, 3.5583322693103924`*^9, {
   3.5642309769239855`*^9, 3.564230977766396*^9}}],

Cell[CellGroupData[{

Cell["Initialization", "Subsubsection",
 CellChangeTimes->{{3.558239452032984*^9, 3.558239460051384*^9}, {
  3.5614160705101285`*^9, 3.561416072270229*^9}}],

Cell[BoxData[{
 RowBox[{
  RowBox[{"<<", "LiteRed`"}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"SetDirectory", "[", 
   RowBox[{"NotebookDirectory", "[", "]"}], "]"}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"SetDim", "[", "d", "]"}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"Declare", "[", 
   RowBox[{
    RowBox[{"{", 
     RowBox[{"l", ",", "r", ",", "k", ",", "p", ",", "q"}], "}"}], ",", 
    "Vector"}], "]"}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"sp", "[", 
    RowBox[{"p", ",", "p"}], "]"}], "=", "0"}], ";", 
  RowBox[{
   RowBox[{"sp", "[", 
    RowBox[{"q", ",", "q"}], "]"}], "=", "0"}], ";", 
  RowBox[{
   RowBox[{"sp", "[", 
    RowBox[{"p", ",", "q"}], "]"}], "=", 
   RowBox[{
    RowBox[{"-", "1"}], "/", "2"}]}], ";"}]}], "Input",
 CellChangeTimes->{{3.5614161561050243`*^9, 3.5614162210277376`*^9}, {
   3.561931271611288*^9, 3.5619312733763885`*^9}, 3.56193208183263*^9, {
   3.562123125987213*^9, 3.5621231272502856`*^9}, {3.562279953983348*^9, 
   3.562279956934517*^9}, {3.56238301621017*^9, 3.5623830176912546`*^9}, {
   3.5624718149626484`*^9, 3.562471815074655*^9}, 3.562548953446727*^9, {
   3.56254947590761*^9, 3.562549476484643*^9}, {3.564109993306654*^9, 
   3.564109997378261*^9}}]
}, Open  ]],

Cell[CellGroupData[{

Cell["Defining the basis and searching for the symmetries", "Subsubsection",
 CellChangeTimes->{{3.558239452032984*^9, 3.558239495198184*^9}}],

Cell[BoxData[{
 RowBox[{
  RowBox[{
   RowBox[{"NewBasis", "[", 
    RowBox[{"v3", ",", 
     RowBox[{"{", 
      RowBox[{"r", ",", 
       RowBox[{"2", 
        RowBox[{"p", "\[CenterDot]", "l"}]}], ",", 
       RowBox[{"2", " ", 
        RowBox[{"q", "\[CenterDot]", "k"}]}], ",", 
       RowBox[{"r", "+", "p"}], ",", 
       RowBox[{"r", "-", "q"}], ",", "k", ",", 
       RowBox[{"k", "-", "r"}], ",", "l", ",", 
       RowBox[{"l", "+", "r"}], ",", 
       RowBox[{"k", "+", "p"}], ",", 
       RowBox[{"l", "+", "r", "-", "k"}], ",", 
       RowBox[{"l", "+", "q"}]}], "}"}], ",", 
     RowBox[{"{", 
      RowBox[{"r", ",", "k", ",", "l"}], "}"}], ",", 
     RowBox[{"Directory", "\[Rule]", "\"\<v3 dir\>\""}]}], "]"}], ";"}], 
  RowBox[{"(*", 
   RowBox[{"Basis", " ", 
    RowBox[{"definition", ".", " ", 
     RowBox[{"sp", "[", 
      RowBox[{"p", ",", "r"}], "]"}]}], " ", "is", " ", "the", " ", 
    "irreducible", " ", 
    RowBox[{"numerator", "."}]}], "*)"}]}], "\n", 
 RowBox[{
  RowBox[{"GenerateIBP", "[", "v3", "]"}], "\n", 
  RowBox[{"(*", 
   RowBox[{
    RowBox[{"IBP", " ", 
     RowBox[{"generation", ".", " ", "Can"}], " ", "be", " ", "also", " ", 
     "invoked", " ", "by", " ", "passing", " ", "GenerateIBP"}], "\[Rule]", 
    RowBox[{
    "True", " ", "option", " ", "in", " ", "NewBasis", " ", "call"}]}], 
   "*)"}]}], "\n", 
 RowBox[{
  RowBox[{"AnalyzeSectors", "[", 
   RowBox[{"v3", ",", 
    RowBox[{"{", 
     RowBox[{"0", ",", "0", ",", "0", ",", "__"}], "}"}]}], "]"}], 
  RowBox[{"(*", 
   RowBox[{
    RowBox[{"Zero", " ", "and", " ", "simple", " ", "sectors", " ", 
     RowBox[{"determination", ".", " ", "Can"}], " ", "be", " ", "also", " ", 
     "invoked", " ", "by", " ", "passing", " ", "AnalyzeSectors"}], "\[Rule]", 
    RowBox[{"True", " ", "option", " ", "in", " ", "NewBasis", " ", 
     RowBox[{"call", ".", " ", "The"}], " ", "second", " ", "argument", " ", 
     "is", " ", "a", " ", "pattern", " ", "which", " ", "claims", " ", "that",
      " ", "the", " ", "sectors", " ", "with", " ", 
     RowBox[{"sp", "[", 
      RowBox[{"p", ",", "r"}], "]"}], " ", "in", " ", "the", " ", 
     "denominator", " ", "should", " ", "not", " ", "be", " ", "analyzed"}]}],
    "*)"}]}], "\n", 
 RowBox[{
  RowBox[{"FindSymmetries", "[", "v3", "]"}], 
  RowBox[{"(*", 
   RowBox[{
    RowBox[{"Finding", " ", "unique", " ", "and", " ", "mapped", " ", 
     RowBox[{"sectors", ".", " ", "Can"}], " ", "be", " ", "also", " ", 
     "invoked", " ", "by", " ", "passing", " ", "FindSymmetries"}], "\[Rule]", 
    RowBox[{"True", " ", "option", " ", "in", " ", "NewBasis", " ", 
     RowBox[{"call", "."}]}]}], "*)"}]}]}], "Input",
 CellChangeTimes->{{3.556387476025002*^9, 3.556387500110939*^9}, {
   3.55638765826154*^9, 3.556387681567492*^9}, 3.5563877956979647`*^9, 
   3.5563878376330256`*^9, {3.556393461976188*^9, 3.556393465660188*^9}, {
   3.556477464449*^9, 3.5564774810950003`*^9}, 3.5578233405741854`*^9, {
   3.5581571953576*^9, 3.5581571978486*^9}, 3.5581614846286*^9, {
   3.5581615270416*^9, 3.5581616128456*^9}, {3.5581618785906*^9, 
   3.5581618843526*^9}, {3.5581620437536*^9, 3.5581621649105997`*^9}, {
   3.5581622432216*^9, 3.5581622527846003`*^9}, {3.5581730390106*^9, 
   3.5581730393276*^9}, {3.5581949609229774`*^9, 3.558195003807378*^9}, {
   3.558238528485384*^9, 3.558238801266984*^9}, {3.5582389073901834`*^9, 
   3.558239015186184*^9}, {3.558239045450184*^9, 3.558239287889784*^9}, 
   3.5582393244405837`*^9, 3.558239440582584*^9, {3.5582880548588*^9, 
   3.5582881676624002`*^9}, 3.558288201998*^9, {3.5582886210316*^9, 
   3.558288633028*^9}, {3.558288666725*^9, 3.5582886711242*^9}, {
   3.558289428561*^9, 3.5582894409474*^9}, {3.5582897957894*^9, 
   3.5582898172756*^9}, {3.5613788903588*^9, 3.5613789125122004`*^9}, {
   3.5613806402153997`*^9, 3.5613806486404*^9}, {3.5619352869349513`*^9, 
   3.561935296687509*^9}, {3.561938038104309*^9, 3.561938045575736*^9}, {
   3.5619385727198873`*^9, 3.56193858360751*^9}, {3.561938807289304*^9, 
   3.561938851207816*^9}, {3.561939957649101*^9, 3.5619399996045003`*^9}, {
   3.5620300894615116`*^9, 3.5620300923726783`*^9}, {3.5620301951075544`*^9, 
   3.5620301984837475`*^9}, {3.562033605492617*^9, 3.5620336384105*^9}, {
   3.562037711319457*^9, 3.562037719804942*^9}, {3.5620385415419436`*^9, 
   3.5620385591249485`*^9}, {3.5622799673981156`*^9, 
   3.5622799913324842`*^9}, {3.5623830300539618`*^9, 3.562383044076764*^9}, {
   3.5624727280068717`*^9, 3.5624727562054844`*^9}, {3.5624746654046845`*^9, 
   3.562474680722561*^9}, {3.56254974393194*^9, 3.5625497477401576`*^9}, {
   3.562641071655337*^9, 3.562641073584447*^9}, {3.5641103794241323`*^9, 
   3.5641103862257442`*^9}}]
}, Open  ]],

Cell[CellGroupData[{

Cell["Searching for the reduction rules", "Subsubsection",
 CellChangeTimes->{{3.558239452032984*^9, 3.5582395181301837`*^9}, 
   3.564110397442164*^9}],

Cell[BoxData[
 RowBox[{"Timing", "[", 
  RowBox[{"SolvejSector", "/@", 
   RowBox[{"UniqueSectors", "[", "v3", "]"}]}], "]"}]], "Input",
 CellChangeTimes->{{3.558255051652384*^9, 3.5582550587883835`*^9}, {
   3.5582895059224*^9, 3.558289507576*^9}, {3.558677655536543*^9, 
   3.5586776562885857`*^9}, 3.5604752809086723`*^9, {3.5611022005652*^9, 
   3.5611022018392*^9}, {3.561350151769159*^9, 3.5613501947796187`*^9}, {
   3.5614163042714987`*^9, 3.5614163102238393`*^9}, {3.5617035841869*^9, 
   3.5617035926143827`*^9}, {3.561703887587254*^9, 3.5617038957777224`*^9}, {
   3.562549187003085*^9, 3.5625491934684553`*^9}, {3.5626414436826153`*^9, 
   3.5626414438276243`*^9}, {3.564110368722513*^9, 3.5641103737457223`*^9}}]
}, Open  ]],

Cell[CellGroupData[{

Cell["Graphs", "Subsubsection",
 CellChangeTimes->{{3.558239452032984*^9, 3.558239460051384*^9}, {
  3.558239531858184*^9, 3.558239532950184*^9}}],

Cell[BoxData[{
 RowBox[{
  RowBox[{
   RowBox[{"AttachGraph", "[", 
    RowBox[{
     RowBox[{"js", "[", 
      RowBox[{
      "v3", ",", "0", ",", "0", ",", "0", ",", "1", ",", "1", ",", "1", ",", 
       "1", ",", "1", ",", "1", ",", "1", ",", "1", ",", "1"}], "]"}], ",", 
     RowBox[{"{", "\[IndentingNewLine]", 
      RowBox[{
       RowBox[{"{", 
        RowBox[{
         RowBox[{"5", "\[Rule]", "7"}], ",", "\"\<0\>\""}], "}"}], ",", 
       "\[IndentingNewLine]", 
       RowBox[{"{", 
        RowBox[{
         RowBox[{"6", "\[Rule]", "7"}], ",", "\"\<0\>\""}], "}"}], ",", 
       "\[IndentingNewLine]", 
       RowBox[{"{", 
        RowBox[{
         RowBox[{"1", "\[Rule]", "3"}], ",", "\"\<0\>\""}], "}"}], ",", 
       "\[IndentingNewLine]", 
       RowBox[{"{", 
        RowBox[{
         RowBox[{"4", "\[Rule]", "5"}], ",", "\"\<0\>\""}], "}"}], ",", 
       "\[IndentingNewLine]", 
       RowBox[{"{", 
        RowBox[{
         RowBox[{"2", "\[Rule]", "4"}], ",", "\"\<0\>\""}], "}"}], ",", 
       "\[IndentingNewLine]", 
       RowBox[{"{", 
        RowBox[{
         RowBox[{"3", "\[Rule]", "6"}], ",", "\"\<0\>\""}], "}"}], ",", 
       "\[IndentingNewLine]", 
       RowBox[{"{", 
        RowBox[{
         RowBox[{"1", "\[Rule]", "5"}], ",", "\"\<0\>\""}], "}"}], ",", 
       "\[IndentingNewLine]", 
       RowBox[{"{", 
        RowBox[{
         RowBox[{"3", "\[Rule]", "4"}], ",", "\"\<0\>\""}], "}"}], ",", 
       "\[IndentingNewLine]", 
       RowBox[{"{", 
        RowBox[{
         RowBox[{"2", "\[Rule]", "6"}], ",", "\"\<0\>\""}], "}"}], ",", 
       "\[IndentingNewLine]", 
       RowBox[{"{", 
        RowBox[{
         RowBox[{"0", "\[Rule]", "1"}], ",", "\"\<p\>\""}], "}"}], ",", 
       "\[IndentingNewLine]", 
       RowBox[{"{", 
        RowBox[{
         RowBox[{"0", "\[Rule]", "2"}], ",", "\"\<q\>\""}], "}"}], ",", 
       "\[IndentingNewLine]", 
       RowBox[{"{", 
        RowBox[{
         RowBox[{"7", "\[Rule]", "0"}], ",", "\"\<p+q\>\""}], "}"}]}], 
      "\[IndentingNewLine]", "}"}]}], "]"}], ";"}], 
  RowBox[{"(*", 
   RowBox[{
   "Attach", " ", "the", " ", "graph", " ", "to", " ", "the", " ", "highest", 
    " ", "sector", " ", 
    RowBox[{"possible", ".", " ", "The"}], " ", "graphs", " ", "for", " ", 
    "the", " ", "subsectors", " ", "are", " ", "determined", " ", 
    RowBox[{"automatically", ".", " ", "The"}], " ", "external", " ", "legs", 
    " ", "should", " ", 
    RowBox[{"start", "/", "end"}], " ", "at", " ", "vertex", " ", "number", 
    " ", "0.", " ", "The", " ", "internal", " ", "vertices", " ", "should", 
    " ", "be", " ", "positive", " ", 
    RowBox[{"integer", ".", " ", "The"}], " ", "order", " ", "of", " ", "the",
     " ", "internal", " ", "lines", " ", "should", " ", "correspond", " ", 
    "to", " ", "the", " ", "order", " ", "of", " ", "denominators", " ", "in",
     " ", "the", " ", 
    RowBox[{"sector", "."}]}], "*)"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"SetOptions", "[", 
   RowBox[{"GraphPlot", ",", 
    RowBox[{"MultiedgeStyle", "\[Rule]", "0.3"}], ",", 
    RowBox[{"VertexRenderingFunction", "\[Rule]", 
     RowBox[{"(", 
      RowBox[{
       RowBox[{"{", 
        RowBox[{
         RowBox[{"If", "[", 
          RowBox[{
           RowBox[{"#2", ">", "0"}], ",", "Black", ",", "Blue"}], "]"}], ",", 
         RowBox[{"Disk", "[", 
          RowBox[{"#1", ",", "0.03"}], "]"}]}], "}"}], "&"}], ")"}]}], ",", 
    RowBox[{"EdgeRenderingFunction", "\[Rule]", 
     RowBox[{"(", 
      RowBox[{
       RowBox[{"Replace", "[", 
        RowBox[{"#3", ",", 
         RowBox[{"{", "\[IndentingNewLine]", 
          RowBox[{
           RowBox[{"\"\<1\>\"", "\[Rule]", 
            RowBox[{"{", 
             RowBox[{"Thick", ",", "Black", ",", 
              RowBox[{"Line", "[", "#1", "]"}]}], "}"}]}], ",", 
           "\[IndentingNewLine]", 
           RowBox[{"\"\<0\>\"", "\[Rule]", 
            RowBox[{"{", 
             RowBox[{"Thin", ",", "Black", ",", "Dashed", ",", 
              RowBox[{"Line", "[", "#1", "]"}]}], "}"}]}], ",", 
           "\[IndentingNewLine]", 
           RowBox[{"\"\<p\>\"", "\[Rule]", 
            RowBox[{"{", 
             RowBox[{
              RowBox[{"Thickness", "[", "0.01", "]"}], ",", "Blue", ",", 
              RowBox[{"Line", "[", "#1", "]"}]}], "}"}]}], ",", 
           "\[IndentingNewLine]", 
           RowBox[{"\"\<q\>\"", "\[Rule]", 
            RowBox[{"{", 
             RowBox[{
              RowBox[{"Thickness", "[", "0.01", "]"}], ",", "Blue", ",", 
              RowBox[{"Line", "[", "#1", "]"}]}], "}"}]}], ",", 
           "\[IndentingNewLine]", 
           RowBox[{"\"\<p+q\>\"", "\[Rule]", 
            RowBox[{"{", 
             RowBox[{
              RowBox[{"Thickness", "[", "0.01", "]"}], ",", "Red", ",", 
              RowBox[{"Line", "[", "#1", "]"}]}], "}"}]}], ",", 
           "\[IndentingNewLine]", 
           RowBox[{"_", "\[Rule]", 
            RowBox[{"Line", "[", "#1", "]"}]}]}], "\[IndentingNewLine]", 
          "}"}]}], "]"}], "&"}], ")"}]}]}], "]"}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"(*", 
   RowBox[{
   "Embellish", " ", "the", " ", "graph", " ", "output", " ", "by", " ", 
    "standard", " ", "Mathematica", " ", "means"}], "*)"}]}]}], "Input",
 CellChangeTimes->{{3.556387476025002*^9, 3.556387500110939*^9}, {
   3.55638765826154*^9, 3.556387681567492*^9}, 3.5563877956979647`*^9, 
   3.5563878376330256`*^9, {3.556393461976188*^9, 3.556393465660188*^9}, {
   3.556477464449*^9, 3.5564774810950003`*^9}, 3.5578233405741854`*^9, 
   3.5578233848191853`*^9, {3.5581616433026*^9, 3.5581616492925997`*^9}, {
   3.5581617952995996`*^9, 3.5581618138736*^9}, {3.5581689496476*^9, 
   3.5581689894076*^9}, {3.5581729620156*^9, 3.5581729702226*^9}, 
   3.5581779450106*^9, {3.5582395409997835`*^9, 3.558239788622384*^9}, {
   3.55829039017*^9, 3.558290610598*^9}, {3.562549284680672*^9, 
   3.5625492856317263`*^9}}],

Cell[BoxData[
 RowBox[{"GraphPlot", "/@", 
  RowBox[{"jGraph", "/@", 
   RowBox[{"MIs", "[", "v3", "]"}], 
   RowBox[{"(*", 
    RowBox[{"Draw", " ", "all", " ", "masters"}], "*)"}]}]}]], "Input",
 CellChangeTimes->{{3.5582399743113837`*^9, 3.558239984585384*^9}, {
  3.5583213581428003`*^9, 3.5583213590008*^9}}]
}, Open  ]],

Cell[CellGroupData[{

Cell[" Saving", "Subsubsection",
 CellChangeTimes->{{3.564110413542592*^9, 3.5641104177078*^9}}],

Cell[BoxData[{
 RowBox[{
  RowBox[{"DiskSave", "[", "v3", "]"}], 
  RowBox[{"(*", 
   RowBox[{"Saving", "."}], "*)"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"Quit", "[", "]"}], 
  RowBox[{"(*", 
   RowBox[{"Quitting", " ", "the", " ", "kernel"}], "*)"}]}]}], "Input",
 CellChangeTimes->{{3.556387476025002*^9, 3.556387500110939*^9}, {
   3.55638765826154*^9, 3.556387681567492*^9}, 3.5563877956979647`*^9, 
   3.5563878376330256`*^9, {3.556393461976188*^9, 3.556393465660188*^9}, {
   3.556477464449*^9, 3.5564774810950003`*^9}, 3.5578233405741854`*^9, {
   3.5581571953576*^9, 3.5581571978486*^9}, 3.5581614846286*^9, {
   3.5581615270416*^9, 3.5581616128456*^9}, {3.5581618785906*^9, 
   3.5581618843526*^9}, {3.5581620437536*^9, 3.5581621649105997`*^9}, {
   3.5581622432216*^9, 3.5581622527846003`*^9}, {3.5581730390106*^9, 
   3.5581730393276*^9}, {3.5581949609229774`*^9, 3.558195003807378*^9}, {
   3.558238528485384*^9, 3.558238801266984*^9}, {3.5582389073901834`*^9, 
   3.558239015186184*^9}, {3.558239045450184*^9, 3.558239287889784*^9}, 
   3.5582393244405837`*^9, 3.558239440582584*^9, {3.558247031471384*^9, 
   3.558247033518384*^9}, {3.5582581804033837`*^9, 3.5582581881233835`*^9}, 
   3.558259951983384*^9, {3.5582888869884*^9, 3.5582888889695997`*^9}, {
   3.558455254359192*^9, 3.5584552563321915`*^9}, {3.560467515358508*^9, 
   3.5604675156455245`*^9}, 3.5611022198199997`*^9, {3.56135414328446*^9, 
   3.5613541444435263`*^9}, {3.5625492429722867`*^9, 3.562549243923341*^9}, 
   3.564110422247408*^9}]
}, Open  ]]
}, Open  ]],

Cell[CellGroupData[{

Cell["Applying", "Section",
 CellChangeTimes->{{3.5581950842253776`*^9, 3.558195085379778*^9}, {
  3.564110331360448*^9, 3.5641103334196515`*^9}}],

Cell[CellGroupData[{

Cell["Initialization", "Subsubsection",
 CellChangeTimes->{{3.558239452032984*^9, 3.558239460051384*^9}}],

Cell[BoxData[{
 RowBox[{
  RowBox[{"<<", "LiteRed`"}], ";"}], "\n", 
 RowBox[{
  RowBox[{"SetDirectory", "[", 
   RowBox[{"NotebookDirectory", "[", "]"}], "]"}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"SetDim", "[", "d", "]"}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"Declare", "[", 
   RowBox[{
    RowBox[{"{", 
     RowBox[{"l", ",", "r", ",", "k", ",", "p", ",", "q"}], "}"}], ",", 
    "Vector"}], "]"}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{
   RowBox[{"sp", "[", 
    RowBox[{"p", ",", "p"}], "]"}], "=", "0"}], ";", 
  RowBox[{
   RowBox[{"sp", "[", 
    RowBox[{"q", ",", "q"}], "]"}], "=", "0"}], ";", 
  RowBox[{
   RowBox[{"sp", "[", 
    RowBox[{"p", ",", "q"}], "]"}], "=", 
   RowBox[{
    RowBox[{"-", "1"}], "/", "2"}]}], ";"}]}], "Input",
 CellChangeTimes->{{3.556387476025002*^9, 3.556387500110939*^9}, {
   3.55638765826154*^9, 3.556387681567492*^9}, 3.5563877956979647`*^9, 
   3.5563878376330256`*^9, {3.556393461976188*^9, 3.556393465660188*^9}, {
   3.556477464449*^9, 3.5564774810950003`*^9}, 3.5578233405741854`*^9, {
   3.5581571953576*^9, 3.5581571978486*^9}, 3.5581614846286*^9, {
   3.5581615270416*^9, 3.5581616128456*^9}, {3.5581618785906*^9, 
   3.5581618843526*^9}, {3.5581620437536*^9, 3.5581621649105997`*^9}, {
   3.5581622432216*^9, 3.5581622527846003`*^9}, {3.5581730390106*^9, 
   3.5581730393276*^9}, {3.5581949609229774`*^9, 3.558195003807378*^9}, {
   3.558238528485384*^9, 3.558238801266984*^9}, {3.5582389073901834`*^9, 
   3.558239015186184*^9}, {3.558239045450184*^9, 3.558239287889784*^9}, {
   3.5582393244405837`*^9, 3.558239326983384*^9}, {3.558288002317*^9, 
   3.5582880270741997`*^9}, {3.5584552656181917`*^9, 3.5584552803431916`*^9}, 
   3.5625492522538176`*^9, {3.564110028158315*^9, 3.564110045380746*^9}}],

Cell[BoxData[
 RowBox[{
  RowBox[{"<<", "\"\<v3 dir/v3\>\""}], ";"}]], "Input",
 CellChangeTimes->{{3.5581950872361774`*^9, 3.558195098982978*^9}, {
   3.5582393598369837`*^9, 3.558239417073384*^9}, {3.5582760384370003`*^9, 
   3.558276038749*^9}, {3.5583213955878*^9, 3.5583214110629997`*^9}, 
   3.5584552596141915`*^9, {3.560467519557748*^9, 3.5604675197967615`*^9}, {
   3.5611169522878*^9, 3.561116953723*^9}, 3.562473977721351*^9, 
   3.5625492672436748`*^9, 3.5626270894456005`*^9, 3.562641473634329*^9, {
   3.5641100674703846`*^9, 3.5641100682815857`*^9}, 3.564110157139342*^9}],

Cell[BoxData[{
 RowBox[{"Timing", "[", 
  RowBox[{"IBPReduce", "[", 
   RowBox[{"LoweringDRR", "[", 
    RowBox[{
    "v3", ",", "0", ",", "0", ",", "0", ",", "1", ",", "1", ",", "1", ",", 
     "1", ",", "1", ",", "1", ",", "1", ",", "1", ",", "1"}], "]"}], "]"}], 
  "]"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"(*", 
   RowBox[{
    RowBox[{"takes", " ", "1"}], "-", 
    RowBox[{"1.5", " ", "hours"}]}], "*)"}]}]}], "Input",
 CellChangeTimes->{{3.5641105487480297`*^9, 3.5641106180433517`*^9}, {
  3.564114687655101*^9, 3.5641146983099194`*^9}}],

Cell[BoxData[{
 RowBox[{"Timing", "[", 
  RowBox[{"IBPReduce", "[", 
   RowBox[{"RaisingDRR", "[", 
    RowBox[{
    "v3", ",", "0", ",", "0", ",", "0", ",", "1", ",", "1", ",", "1", ",", 
     "1", ",", "1", ",", "1", ",", "1", ",", "1", ",", "1"}], "]"}], "]"}], 
  "]"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"(*", 
   RowBox[{"takes", " ", "about", " ", "1", " ", "hour"}], "*)"}]}]}], "Input",
 CellChangeTimes->{{3.5641144197372303`*^9, 3.564114420876032*^9}, {
  3.5641198101578984`*^9, 3.564119810454299*^9}}]
}, Open  ]]
}, Open  ]]
}, Open  ]]
},
WindowSize->{707, 549},
WindowMargins->{{89, Automatic}, {18, Automatic}},
FrontEndVersion->"8.0 for Microsoft Windows (32-bit) (February 23, 2011)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[CellGroupData[{
Cell[579, 22, 237, 3, 83, "Title"],
Cell[819, 27, 5257, 154, 271, "Input"],
Cell[6079, 183, 129, 1, 29, "Text"],
Cell[CellGroupData[{
Cell[6233, 188, 464, 6, 71, "Section"],
Cell[CellGroupData[{
Cell[6722, 198, 156, 2, 27, "Subsubsection"],
Cell[6881, 202, 1279, 33, 132, "Input"]
}, Open  ]],
Cell[CellGroupData[{
Cell[8197, 240, 142, 1, 27, "Subsubsection"],
Cell[8342, 243, 4707, 93, 152, "Input"]
}, Open  ]],
Cell[CellGroupData[{
Cell[13086, 341, 152, 2, 27, "Subsubsection"],
Cell[13241, 345, 725, 11, 31, "Input"]
}, Open  ]],
Cell[CellGroupData[{
Cell[14003, 361, 146, 2, 27, "Subsubsection"],
Cell[14152, 365, 5942, 142, 592, "Input"],
Cell[20097, 509, 313, 7, 31, "Input"]
}, Open  ]],
Cell[CellGroupData[{
Cell[20447, 521, 96, 1, 27, "Subsubsection"],
Cell[20546, 524, 1532, 26, 52, "Input"]
}, Open  ]]
}, Open  ]],
Cell[CellGroupData[{
Cell[22127, 556, 146, 2, 71, "Section"],
Cell[CellGroupData[{
Cell[22298, 562, 105, 1, 27, "Subsubsection"],
Cell[22406, 565, 1804, 40, 132, "Input"],
Cell[24213, 607, 587, 9, 31, "Input"],
Cell[24803, 618, 556, 14, 52, "Input"],
Cell[25362, 634, 524, 12, 52, "Input"]
}, Open  ]]
}, Open  ]]
}, Open  ]]
}
]
*)

(* End of internal cache information *)

