(* Content-type: application/vnd.wolfram.mathematica *)

(*** Wolfram Notebook File ***)
(* http://www.wolfram.com/nb *)

(* CreatedBy='Mathematica 8.0' *)

(*CacheID: 234*)
(* Internal cache information:
NotebookFileLineBreakTest
NotebookFileLineBreakTest
NotebookDataPosition[       157,          7]
NotebookDataLength[     14773,        364]
NotebookOptionsPosition[     13830,        328]
NotebookOutlinePosition[     14176,        343]
CellTagsIndexPosition[     14133,        340]
WindowFrame->Normal*)

(* Beginning of Notebook Content *)
Notebook[{

Cell[CellGroupData[{
Cell["Four-loop massive tadpole", "Title",
 CellChangeTimes->{{3.554301465671934*^9, 3.5543014667206793`*^9}, {
  3.5563005990081844`*^9, 3.5563006152061844`*^9}, {3.558332248952392*^9, 
  3.558332251307992*^9}, {3.5590408683344*^9, 3.5590408691612*^9}, {
  3.5594368907855806`*^9, 3.559436893589741*^9}}],

Cell[BoxData[
 GraphicsBox[{
   {AbsoluteThickness[3], StrokeForm[{GrayLevel[0.], Opacity[1.]}], EdgeForm[{
    GrayLevel[0.], Opacity[1.]}], EdgeForm[None], 
    CircleBox[{0.6666666666666667, 0.5000000000000001}, \
{0.3145469048041277, 0.3145469048041276}]}, 
   {AbsoluteThickness[1], Dashing[{Medium, Medium}], StrokeForm[{GrayLevel[
    0.], Opacity[1.]}], EdgeForm[{GrayLevel[0.], Opacity[1.]}], EdgeForm[
    None], LineBox[{{0.5444444444444446, 0.7923246825819052}, {
     0.6666666666666669, 0.18545309519587216`}}]}, 
   {AbsoluteThickness[1], Dashing[{Medium, Medium}], StrokeForm[{GrayLevel[
    0.], Opacity[1.]}], EdgeForm[{GrayLevel[0.], Opacity[1.]}], EdgeForm[
    None], LineBox[{{0.3916666666666667, 0.6527777777777779}, {
     0.9784357936930167, 0.46944444444444433`}}]}, 
   {AbsoluteThickness[1], Dashing[{Medium, Medium}], StrokeForm[{GrayLevel[
    0.], Opacity[1.]}], EdgeForm[{GrayLevel[0.], Opacity[1.]}], EdgeForm[
    None], LineBox[{{0.45000000000000007`, 0.2694444444444445}, {
     0.9500000000000002, 0.6305555555555554}}]}, 
   {AbsolutePointSize[6], PointBox[{0.3916666666666667, 0.6527777777777779}]}, 
   {AbsolutePointSize[6], PointBox[{0.5444444444444446, 0.7923246825819052}]}, 
   {AbsolutePointSize[6], PointBox[{0.9500000000000002, 0.6305555555555554}]}, 
   {AbsolutePointSize[6], 
    PointBox[{0.45000000000000007`, 0.2694444444444445}]}, 
   {AbsolutePointSize[6], 
    PointBox[{0.6666666666666669, 0.18545309519587216`}]}, 
   {AbsolutePointSize[6], 
    PointBox[{0.9812135714707946, 0.46944444444444433`}]}, InsetBox[
    StyleBox[
     TagBox["2",
      HoldForm],
     TextAlignment->Center,
     Magnification->1.6000001430511475`,
     Background->GrayLevel[1.]], {0.5290746077489594, 0.17530419468459824`}, {
    Left, Baseline},
    Alignment->{Left, Top}], InsetBox[
    StyleBox[
     TagBox["3",
      HoldForm],
     TextAlignment->Center,
     Magnification->1.6000001430511475`,
     Background->GrayLevel[1.]], {0.8850464297150178, 0.2483109189881526}, {
    Left, Baseline},
    Alignment->{Left, Top}], InsetBox[
    StyleBox[
     TagBox["4",
      HoldForm],
     TextAlignment->Center,
     Magnification->1.6000001430511475`,
     Background->GrayLevel[1.]], {0.9673246825819056, 0.5402777777777779}, {
    Left, Baseline},
    Alignment->{Left, Top}], InsetBox[
    StyleBox[
     TagBox["5",
      HoldForm],
     TextAlignment->Center,
     Magnification->1.6000001430511475`,
     Background->GrayLevel[1.]], {0.7861111111111113, 0.7569100171960585}, {
    Left, Baseline},
    Alignment->{Left, Top}], InsetBox[
    StyleBox[
     TagBox["6",
      HoldForm],
     TextAlignment->Center,
     Magnification->1.6000001430511475`,
     Background->GrayLevel[1.]], {0.45, 0.7242691270263495}, {Left, Baseline},
    Alignment->{Left, Top}], InsetBox[
    StyleBox[
     TagBox["7",
      HoldForm],
     TextAlignment->Center,
     Magnification->1.6000001430511475`,
     Background->GrayLevel[
      1.]], {0.34100865075142783`, 0.43393772014088994`}, {
    Left, Baseline}, {0.02222222222222223, 0.052777777777777785`}, {{1., 
    0.}, {0., 1.}},
    Alignment->{Left, Top}], InsetBox[
    StyleBox[
     TagBox["8",
      HoldForm],
     TextAlignment->Center,
     Magnification->1.6000001430511475`,
     Background->GrayLevel[1.]], {0.5916666666666668, 0.4805555555555553}, {
    Left, Baseline},
    Alignment->{Left, Top}], InsetBox[
    StyleBox[
     TagBox["9",
      HoldForm],
     TextAlignment->Center,
     Magnification->1.6000001430511475`,
     Background->GrayLevel[1.]], {0.711295228946526, 0.43789225104066565`}, {
    Left, Baseline},
    Alignment->{Left, Top}], InsetBox[
    StyleBox[
     TagBox["10",
      HoldForm],
     TextAlignment->Center,
     Magnification->1.6000001430511475`,
     Background->GrayLevel[1.]], {0.6666666666666666, 0.5402777777777779}, {
    Left, Baseline},
    Alignment->{Left, Top}]},
  ContentSelectable->True,
  ImagePadding->{{0., 0.}, {0., 0.}},
  ImageSize->{347., 262.},
  PlotRange->{{0., 1.3333333333333335`}, {0., 1.}},
  PlotRangePadding->Automatic]], "Input",
 CellChangeTimes->{{3.559436909339642*^9, 3.559436930789869*^9}, {
  3.559436989429223*^9, 3.5594371002215595`*^9}, {3.559437417273694*^9, 
  3.5594374945061116`*^9}, {3.5594375341573796`*^9, 3.559437610985774*^9}, {
  3.5641828743241105`*^9, 3.5641829397062025`*^9}}],

Cell[CellGroupData[{

Cell["Finding reduction rules", "Section",
 CellChangeTimes->{{3.5563006288211846`*^9, 3.5563006412801847`*^9}, 
   3.5563047894731846`*^9, {3.5563874491311193`*^9, 3.5563874694263287`*^9}, {
   3.556387654361615*^9, 3.556387654580011*^9}, {3.5581615194226*^9, 
   3.5581615214086*^9}, {3.5581950641949778`*^9, 3.5581950794205775`*^9}, 
   3.558332256112792*^9, {3.5642309963462343`*^9, 3.5642309972354455`*^9}}],

Cell[CellGroupData[{

Cell["Initialization", "Subsubsection",
 CellChangeTimes->{{3.558239452032984*^9, 3.558239460051384*^9}}],

Cell[BoxData[{
 RowBox[{
  RowBox[{"<<", "LiteRed`"}], ";"}], "\n", 
 RowBox[{
  RowBox[{"SetDirectory", "[", 
   RowBox[{"NotebookDirectory", "[", "]"}], "]"}], 
  ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"SetDim", "[", "d", "]"}], ";"}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"Declare", "[", 
   RowBox[{
    RowBox[{"{", 
     RowBox[{"k1", ",", "k2", ",", "k3", ",", "k4"}], "}"}], ",", "Vector"}], 
   "]"}], ";"}]}], "Input",
 CellChangeTimes->{{3.556387476025002*^9, 3.556387500110939*^9}, {
   3.55638765826154*^9, 3.556387681567492*^9}, 3.5563877956979647`*^9, 
   3.5563878376330256`*^9, {3.556393461976188*^9, 3.556393465660188*^9}, {
   3.556477464449*^9, 3.5564774810950003`*^9}, 3.5578233405741854`*^9, {
   3.5581571953576*^9, 3.5581571978486*^9}, 3.5581614846286*^9, {
   3.5581615270416*^9, 3.5581616128456*^9}, {3.5581618785906*^9, 
   3.5581618843526*^9}, {3.5581620437536*^9, 3.5581621649105997`*^9}, {
   3.5581622432216*^9, 3.5581622527846003`*^9}, {3.5581730390106*^9, 
   3.5581730393276*^9}, {3.5581949609229774`*^9, 3.558195003807378*^9}, {
   3.558238528485384*^9, 3.558238801266984*^9}, {3.5582389073901834`*^9, 
   3.558239015186184*^9}, {3.558239045450184*^9, 3.558239287889784*^9}, {
   3.5582393244405837`*^9, 3.558239326983384*^9}, {3.5584550621361914`*^9, 
   3.5584551101531916`*^9}, {3.5590408772264*^9, 3.5590408775228*^9}, 
   3.5590429864049997`*^9, {3.564209399337182*^9, 3.564209412347605*^9}}]
}, Open  ]],

Cell[CellGroupData[{

Cell["Defining the basis and searching for the symmetries", "Subsubsection",
 CellChangeTimes->{{3.558239452032984*^9, 3.558239495198184*^9}}],

Cell[BoxData[{
 RowBox[{
  RowBox[{"NewBasis", "[", 
   RowBox[{"t4", ",", 
    RowBox[{"{", 
     RowBox[{
      RowBox[{"k4", "-", "k1"}], ",", 
      RowBox[{
       RowBox[{"k1", "\[CenterDot]", "k1"}], "+", "1"}], ",", 
      RowBox[{
       RowBox[{"k2", "\[CenterDot]", "k2"}], "+", "1"}], ",", 
      RowBox[{
       RowBox[{"k3", "\[CenterDot]", "k3"}], "+", "1"}], ",", 
      RowBox[{
       RowBox[{"k4", "\[CenterDot]", "k4"}], "+", "1"}], ",", 
      RowBox[{
       RowBox[{
        RowBox[{"(", 
         RowBox[{"k1", "-", "k2", "+", "k4"}], ")"}], "\[CenterDot]", 
        RowBox[{"(", 
         RowBox[{"k1", "-", "k2", "+", "k4"}], ")"}]}], "+", "1"}], ",", 
      RowBox[{
       RowBox[{
        RowBox[{"(", 
         RowBox[{"k1", "-", "k3", "+", "k4"}], ")"}], "\[CenterDot]", 
        RowBox[{"(", 
         RowBox[{"k1", "-", "k3", "+", "k4"}], ")"}]}], "+", "1"}], ",", 
      RowBox[{"k1", "-", "k2"}], ",", 
      RowBox[{"k2", "-", "k3"}], ",", 
      RowBox[{"k3", "-", "k4"}]}], "}"}], ",", 
    RowBox[{"{", 
     RowBox[{"k1", ",", "k2", ",", "k3", ",", "k4"}], "}"}], ",", 
    RowBox[{"Directory", "\[Rule]", "\"\<t4 dir\>\""}]}], "]"}], ";"}], "\n", 
 RowBox[{"GenerateIBP", "[", "t4", "]"}], "\n", 
 RowBox[{"AnalyzeSectors", "[", 
  RowBox[{"t4", ",", 
   RowBox[{"{", 
    RowBox[{"0", ",", "__"}], "}"}]}], "]"}], "\n", 
 RowBox[{"FindSymmetries", "[", "t4", "]"}]}], "Input",
 CellChangeTimes->{{3.556387476025002*^9, 3.556387500110939*^9}, {
   3.55638765826154*^9, 3.556387681567492*^9}, 3.5563877956979647`*^9, 
   3.5563878376330256`*^9, {3.556393461976188*^9, 3.556393465660188*^9}, {
   3.556477464449*^9, 3.5564774810950003`*^9}, 3.5578233405741854`*^9, {
   3.5581571953576*^9, 3.5581571978486*^9}, 3.5581614846286*^9, {
   3.5581615270416*^9, 3.5581616128456*^9}, {3.5581618785906*^9, 
   3.5581618843526*^9}, {3.5581620437536*^9, 3.5581621649105997`*^9}, {
   3.5581622432216*^9, 3.5581622527846003`*^9}, {3.5581730390106*^9, 
   3.5581730393276*^9}, {3.5581949609229774`*^9, 3.558195003807378*^9}, {
   3.558238528485384*^9, 3.558238801266984*^9}, {3.5582389073901834`*^9, 
   3.558239015186184*^9}, {3.558239045450184*^9, 3.558239287889784*^9}, 
   3.5582393244405837`*^9, 3.558239440582584*^9, {3.5583292212296*^9, 
   3.5583292265576*^9}, {3.5590408879124002`*^9, 3.5590409191280003`*^9}, {
   3.5590409679872*^9, 3.5590409767544003`*^9}, {3.559042969843*^9, 
   3.559042975743*^9}, {3.559043034133*^9, 3.559043041986*^9}, {
   3.559043280585*^9, 3.5590432922019997`*^9}, {3.559043584316*^9, 
   3.5590436114890003`*^9}, {3.5590478068543997`*^9, 3.5590478307416*^9}, {
   3.5591798064164476`*^9, 3.559179812457793*^9}, {3.5591798434135637`*^9, 
   3.55917984544668*^9}, {3.5591811820591297`*^9, 3.5591811856203337`*^9}, {
   3.559181226491671*^9, 3.5591812291958256`*^9}, {3.5591814363896766`*^9, 
   3.5591814391688356`*^9}, {3.559182484902648*^9, 3.5591824911270037`*^9}, {
   3.559182726483466*^9, 3.559182731259739*^9}, {3.55943737419223*^9, 
   3.559437382102682*^9}, {3.5636040180434113`*^9, 3.563604023519021*^9}, {
   3.563604191079915*^9, 3.5636041965087247`*^9}, {3.5642094160448117`*^9, 
   3.564209423720025*^9}, {3.5642095434044356`*^9, 3.5642095440908365`*^9}}]
}, Open  ]],

Cell[CellGroupData[{

Cell["Searching for the reduction rules and saving", "Subsubsection",
 CellChangeTimes->{{3.558239452032984*^9, 3.5582395181301837`*^9}}],

Cell[BoxData[
 RowBox[{"SetOptions", "[", 
  RowBox[{"SolvejSector", ",", 
   RowBox[{"DiskSave", "\[Rule]", "True"}], ",", 
   RowBox[{"Depth", "\[Rule]", "0"}]}], "]"}]], "Input",
 CellChangeTimes->{{3.563604055717478*^9, 3.563604065732695*^9}, {
  3.5636050805166817`*^9, 3.5636050832934866`*^9}}],

Cell[BoxData[
 RowBox[{"Timing", "[", 
  RowBox[{"SolvejSector", "[", 
   RowBox[{"UniqueSectors", "[", "t4", "]"}], "]"}], "]"}]], "Input",
 CellChangeTimes->{{3.559387002616211*^9, 3.559387014196289*^9}, {
  3.5636041599412603`*^9, 3.563604162406065*^9}, {3.5641829698862443`*^9, 
  3.564182971396247*^9}}],

Cell[BoxData[
 RowBox[{"DiskSave", "[", "t4", "]"}]], "Input",
 CellChangeTimes->{{3.5636619588229675`*^9, 3.563661958916568*^9}}],

Cell[BoxData[
 RowBox[{"Timing", "[", 
  RowBox[{"IBPReduce", "[", 
   RowBox[{"RaisingDRR", "[", 
    RowBox[{
    "t4", ",", " ", "0", ",", " ", "1", ",", " ", "1", ",", " ", "1", ",", 
     " ", "1", ",", " ", "1", ",", " ", "1", ",", " ", "1", ",", " ", "1", 
     ",", " ", "1"}], "]"}], "]"}], "]"}]], "Input",
 CellChangeTimes->{{3.563662029818692*^9, 3.563662042236314*^9}, {
  3.5641919180266404`*^9, 3.5641919197426434`*^9}}],

Cell[BoxData[
 RowBox[{"Timing", "[", 
  RowBox[{"IBPReduce", "[", 
   RowBox[{"LoweringDRR", "[", 
    RowBox[{
    "t4", ",", " ", "0", ",", " ", "1", ",", " ", "1", ",", " ", "1", ",", 
     " ", "1", ",", " ", "1", ",", " ", "1", ",", " ", "1", ",", " ", "1", 
     ",", " ", "1"}], "]"}], "]"}], "]"}]], "Input",
 CellChangeTimes->{{3.5641919311150637`*^9, 3.564191932753066*^9}}],

Cell[BoxData[{
 RowBox[{
  RowBox[{"DiskSave", "[", "t4", "]"}], 
  RowBox[{"(*", 
   RowBox[{"Saving", "."}], "*)"}]}], "\[IndentingNewLine]", 
 RowBox[{
  RowBox[{"Quit", "[", "]"}], 
  RowBox[{"(*", 
   RowBox[{"Quitting", " ", "the", " ", "kernel"}], "*)"}]}]}], "Input",
 CellChangeTimes->{{3.556387476025002*^9, 3.556387500110939*^9}, {
   3.55638765826154*^9, 3.556387681567492*^9}, 3.5563877956979647`*^9, 
   3.5563878376330256`*^9, {3.556393461976188*^9, 3.556393465660188*^9}, {
   3.556477464449*^9, 3.5564774810950003`*^9}, 3.5578233405741854`*^9, {
   3.5581571953576*^9, 3.5581571978486*^9}, 3.5581614846286*^9, {
   3.5581615270416*^9, 3.5581616128456*^9}, {3.5581618785906*^9, 
   3.5581618843526*^9}, {3.5581620437536*^9, 3.5581621649105997`*^9}, {
   3.5581622432216*^9, 3.5581622527846003`*^9}, {3.5581730390106*^9, 
   3.5581730393276*^9}, {3.5581949609229774`*^9, 3.558195003807378*^9}, {
   3.558238528485384*^9, 3.558238801266984*^9}, {3.5582389073901834`*^9, 
   3.558239015186184*^9}, {3.558239045450184*^9, 3.558239287889784*^9}, 
   3.5582393244405837`*^9, 3.558239440582584*^9, {3.558247031471384*^9, 
   3.558247033518384*^9}, {3.5582581804033837`*^9, 3.5582581881233835`*^9}, 
   3.558259951983384*^9, 3.5584550557511916`*^9, {3.559041037924*^9, 
   3.5590410392656*^9}, {3.564193896930846*^9, 3.564193897055647*^9}}]
}, Open  ]]
}, Open  ]]
}, Open  ]]
},
WindowSize->{707, 549},
WindowMargins->{{134, Automatic}, {Automatic, 28}},
FrontEndVersion->"8.0 for Microsoft Windows (32-bit) (February 23, 2011)",
StyleDefinitions->"Default.nb"
]
(* End of Notebook Content *)

(* Internal cache information *)
(*CellTagsOutline
CellTagsIndex->{}
*)
(*CellTagsIndex
CellTagsIndex->{}
*)
(*NotebookFileOutline
Notebook[{
Cell[CellGroupData[{
Cell[579, 22, 305, 4, 83, "Title"],
Cell[887, 28, 4370, 108, 277, "Input"],
Cell[CellGroupData[{
Cell[5282, 140, 412, 5, 71, "Section"],
Cell[CellGroupData[{
Cell[5719, 149, 105, 1, 27, "Subsubsection"],
Cell[5827, 152, 1451, 28, 92, "Input"]
}, Open  ]],
Cell[CellGroupData[{
Cell[7315, 185, 142, 1, 27, "Subsubsection"],
Cell[7460, 188, 3232, 63, 132, "Input"]
}, Open  ]],
Cell[CellGroupData[{
Cell[10729, 256, 137, 1, 27, "Subsubsection"],
Cell[10869, 259, 300, 6, 31, "Input"],
Cell[11172, 267, 308, 6, 31, "Input"],
Cell[11483, 275, 130, 2, 31, "Input"],
Cell[11616, 279, 435, 9, 31, "Input"],
Cell[12054, 290, 385, 8, 31, "Input"],
Cell[12442, 300, 1348, 23, 52, "Input"]
}, Open  ]]
}, Open  ]]
}, Open  ]]
}
]
*)

(* End of internal cache information *)

